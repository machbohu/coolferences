import React from "react";

import { shallow } from "enzyme";

import { Conference } from "../../../src/components/Conference";
import { Conferences } from "../../../src/components/Conferences";
import { ConferenceFactory } from "../../factories/Conference";

it("shows the right number of conferences", () => {
  const NUMBER_OF_CONFERENCES = 4;

  const conferences = Array(NUMBER_OF_CONFERENCES)
    .fill(null)
    .map(() => ConferenceFactory());

  const subject = shallow(<Conferences conferences={conferences} />);

  expect(subject.find(Conference)).toHaveLength(NUMBER_OF_CONFERENCES);
});
