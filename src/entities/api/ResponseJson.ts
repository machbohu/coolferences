import { ConferenceJson } from "./ConferenceJson";

export interface ResponseJson {
  [index: string]: ConferenceJson[];
}
