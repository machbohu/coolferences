import moment from "moment";

import { ConferenceJson } from "../api/ConferenceJson";
import { Conference } from "../Conference";

export type ConferenceFactory = (json: ConferenceJson) => Conference;

export function ConferenceFactoryBuilder({ today }: { today: Date }): ConferenceFactory {
  return ({
    name,
    location,
    date,
    coc: { has_coc: hasCoc, link: cocUrl },
    cfp: { has_cfp: hasCfp, link: cfpUrl },
    url
  }) => {
    return {
      cfpUrl,
      cocUrl,
      date,
      hasCfp,
      hasCoc,
      isConcluded: moment(today).isAfter(moment(date.end, "MM-DD-YYYY")),
      location,
      name,
      url
    };
  };
}
